#include <eosiolib/eosio.hpp>
#include <eosiolib/crypto.h>
#include <eosiolib/singleton.hpp>
#include <eosiolib/transaction.hpp>
#include <string>

using eosio::contract;
using eosio::multi_index;
using eosio::print;
using eosio::singleton;
using eosio::action;
using eosio::permission_level;
using std::string;
using std::make_tuple;

class atomic : contract {
    public:
        atomic(account_name account) : contract(account), accounts(_self, _self) {};

        void initialize(string sign_address,
                account_name receive_account,
                account_name refund_account,
                uint64_t unlock_time,
                uint64_t amount) {
            accounts.set(my_data{sign_address, receive_account, refund_account, unlock_time}, _self);
        };

        void claim(const checksum256 digest, string sig, size_t siglen){
            auto results = accounts.get();
            assert_recover_key(&digest, sig.c_str(), siglen, results.sign_account.c_str(), sizeof(results.sign_account));
            action(
                    permission_level(_self, N(active) ),
                    N(eosio.token), N(transfer),
                    make_tuple(_self, results.receive_account, results.amount, string(""))
                  ).send();
        }

        void refund() {
            require_auth(_self);
            auto results = accounts.get();
            if (tapos_block_num() > results.tapos + results.unlock_time) {
                action(
                        permission_level(_self, N(active) ),
                        N(eosio.token), N(transfer),
                        make_tuple(_self, results.receive_account, results.amount, string(""))
                      ).send();
            }
        }

    private:
        struct my_data {
            string sign_account;
            account_name receive_account;
            account_name refund_account;
            uint64_t unlock_time;
            uint64_t amount;
            uint64_t tapos = tapos_block_num();
        };

        singleton<N(my_data), my_data> accounts;
};

EOSIO_ABI(atomic, (initialize)(claim));
