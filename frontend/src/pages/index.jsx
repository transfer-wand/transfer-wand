import React, { Component } from 'react';
import Eos from 'eosjs'; // https://github.com/EOSIO/eosjs

// material-ui dependencies
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Delete from '@material-ui/icons/Delete';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';
import Lock from '@material-ui/icons/Lock';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';

const privateKey = "5K7mtrinTFrVTduSxizUc5hjXJEtTjVTsqSHeBHes1Viep86FP5"
const WandSvg = require('./wand.svg');

// TODO: get from URL?
const current_user = "bigcorp";

const logos = {
  "bigcorp": "???",
  "supplier": "???",
};

// map from user to set of recipients to display
const recipients = {
  "bigcorp": ["Acme Supplier Co.", "Widget Masters Inc."],
  "supplier": ["Upstream Ltd."],
};


// set up styling classes using material-ui "withStyles"
const styles = theme => ({
  card: {
    margin: 20,
  },
  paper: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      marginTop: theme.spacing.unit * 6,
      marginBottom: theme.spacing.unit * 6,
      padding: theme.spacing.unit * 3,
    },
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  formButton: {
    marginTop: theme.spacing.unit,
    width: "100%",
  },
  pre: {
    background: "#ccc",
    padding: 10,
    marginBottom: 0.
  },
});

class WalletPart extends Component {
  constructor(props) {
    super(props);
    this.state = {recipient: null}
  }

  render() {
    const { classes } = this.props;

    const generateMenuItem = (name, i) => {
      return (
        <MenuItem key={i+1} value={name}>{name}</MenuItem>
      );
    }

    const menuItems = recipients[current_user].map(generateMenuItem);
		menuItems.unshift(<MenuItem key={0} value={this.props.firstRecipient}><em>{this.props.firstRecipient}</em></MenuItem>);

    const recipChange = event => {
      this.setState({recipient: event.target.value});
    };

    return (
      <form onSubmit={this.props.handleFormEvent}>
        <TextField
          id="standard-number"
          label="Amount"
          type="number"
          name="amount"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
          fullWidth
        />

        <TextField fullWidth select
          name="recipient"
          label="Recipient"
          name="recipient"
          value={this.state.recipient || ""}
          onChange={recipChange}
          className={classes.textField}
        >
          {menuItems}
        </TextField>

        <br/>

        <Button align="right" variant="contained" color="primary"
                style={{marginTop: "2em"}}
                type="submit"
        >
          {this.props.buttonText}
        </Button>
      </form>
    );
  }
};

class Index extends Component {
  // Bob's address in case of refund
  //const refund_address = "...";   // TODO: set hardcoded
  //const unlock_time = "12 hours"; // TODO
  //const receive_address = "alice"; // TODO: set hardcoded

  // provide to claim method(?)
  //var sign_address;

  constructor(props) {
    super(props);
    this.state = {
      pub_balance: 4096.0,
      priv_balance: 0,
      current_company: true,
      last_msg: null,
    };

    this.handlePublicFormEvent = this.handlePublicFormEvent.bind(this);
    this.handlePrivateFormEvent = this.handlePrivateFormEvent.bind(this);
    this.swapCompany = this.swapCompany.bind(this);
    this.companyName = this.companyName.bind(this);
  }

  async doTransaction(msg) {
    // --- BEGIN eos stuff
    const account = "useraaaaaaaa"
    const eos = Eos({keyProvider: privateKey})
    const result = await eos.transaction({
      actions: [{
        account: "notechainacc",
        name: "update",
        authorization: [{
          actor: account,
          permission: "active",
        }],
        data: {
          _user: account,
          _note: msg,
        },
      }],
    });
    console.log(result);
  }

  async waitForTransaction() {
  }

  companyName() {
    if(this.state.current_company) {
      return "BIGRETAIL CO";
    } else {
      return "ACME SUPPLIER CO";
    }
  }

  async waitForTransaction() {
    // Hackitty hack!
    console.log("waiting!!!!");
    const eos = Eos({keyProvider: privateKey})
    eos.getTableRows({
      "json": true,
      "code": "notechainacc",
      "scope": "notechainacc",
      "table": "notestruct",
      "limit": 100,
    }).then(result => {
      if(result.rows.length > 0) {
        const msg = result.rows[0].note;
        console.log("msg: ", msg);
        if(msg.startsWith("private transaction") && msg !== this.state.last_msg) {
          this.setState({
            priv_balance: this.state.priv_balance + 10,
            last_msg: msg,
          });
        }
      }
    });
  }

  swapCompany() {
    this.setState({
      current_company: !this.state.current_company,
      pub_balance: 100,
      priv_balance: 0,
    });

    setInterval(() => this.waitForTransaction(), 500);
  }

  async handlePublicFormEvent(event) {
    console.log('handlePublicFormEvent');
    event.preventDefault();
    const amount = parseInt(event.target.amount.value, 10);
    const recipient = event.target.recipient.value;

    await this.doTransaction("public transaction");

    const new_pub_balance = this.state.pub_balance - amount;
    var new_priv_balance = this.state.priv_balance;
    if(recipient === "Private Wallet") {
      new_priv_balance += amount;
    }

    // TODO: after transaction
    this.setState({
      pub_balance: new_pub_balance,
      priv_balance: new_priv_balance,
    });
  }

  async handlePrivateFormEvent(event) {
    console.log('handlePublicFormEvent');
    event.preventDefault();
    const amount = parseInt(event.target.amount.value, 10);
    const recipient = event.target.recipient.value;

    var msg = "private transaction " + Math.random();
    if(!this.state.current_company && recipient == "Public Wallet") {
      msg = "back-to-public transaction";
    }

    await this.doTransaction(msg);

    const new_priv_balance = this.state.priv_balance - amount;
    var new_pub_balance = this.state.pub_balance;
    if(recipient === "Public Wallet") {
      new_pub_balance += amount;
    }

    // TODO: after transaction
    this.setState({
      pub_balance: new_pub_balance,
      priv_balance: new_priv_balance,
    });
  }

  renderBalanceHeading(value, subhead) {
    return (
      <React.Fragment>
        <Typography align="center" variant="display1">
          {value}
        </Typography>
        <Typography align="center" variant="subheading" gutterBottom>
          ({subhead})
        </Typography>
      </React.Fragment>
    );
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <main className={classes.layout}>
					<AppBar
							position="static" color="default"
							className={classes.appBar}
					>
						<Toolbar>
							<Typography variant="title" color="inherit" noWrap style={{flex: 1}}>
								TransferWand
							</Typography>
              <Button color="inherit" onClick={this.swapCompany}>{this.companyName()}</Button>
						</Toolbar>
					</AppBar>
          <Paper className={classes.paper}>
            <Typography align="center" variant="display2" gutterBottom>
              Balances
            </Typography>

            {this.renderBalanceHeading(this.state.pub_balance, "Public")}
            <WalletPart
                classes={classes}
                buttonText="Send"
							  firstRecipient="Private Wallet"
                handleFormEvent={this.handlePublicFormEvent}
            />


            <div style={{marginTop: "2em"}}>
            </div>

            {this.renderBalanceHeading(this.state.priv_balance, "Private")}
            <WalletPart
              classes={classes}
              buttonText="Send Privately"
              firstRecipient="Public Wallet"
              handleFormEvent={this.handlePrivateFormEvent}
            />
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(Index);
